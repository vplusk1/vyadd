<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Subscriber;

class MainController extends Controller
{


    public function sendDocument(Request $request)
    {
        $path = base_path().'/public/doc1.docx';
        $email = $request->email;
        $name = $request->name;

        Mail::send('emails.doc', [], function ($message) use ($email, $path) {
            $message
                ->from(env('MAIL_FROM'), env('MAIL_NAME'))
                ->to($email, env('MAIL_NAME'))
                ->subject('Vyadd | Your document');

            $message->attach($path);

        });

        $subscriber = new Subscriber();
        $subscriber->email = $email;
        $subscriber->name = $name;
        $subscriber->save();
    }

    public function sendMessage(Request $request)
    {
        $message_text = $request->message;
        $subscribers = Subscriber::all();
        // or Subscriber::whereBetween('created_at', [$date_start, $date_end])->get();

        foreach ($subscribers as $subscriber) {
            Mail::send('emails.message', ['message_text' => $message_text, 'name' => $subscriber->name], function ($message) use ($subscriber) {
                $message
                    ->from(env('MAIL_FROM'), env('MAIL_NAME'))
                    ->to($subscriber->email, env('MAIL_NAME'))
                    ->subject('Vyadd | Hello!');
            });
        }
    }

}
