<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>
<body>
<header class="header">
    <div class="container">
        <div id="hamburger" class="hamburger hamburger--emphatic js-hamburger"><div class="hamburger-box"><div class="hamburger-inner"></div></div></div>
        <div class="nav-wrap">
            <nav class="nav" id="nav">
                <a href="#about" class="nav-link">About</a>
                <a href="#publishers" class="nav-link">For publishers</a>
                <a href="#advertisers" class="nav-link">For advertisers</a>
                <a href="#contact" class="nav-link">Contact</a>
                <a href="#signup" class="nav-link">Sign up</a>
            </nav>
        </div>
    </div>
</header>
<main class="main">
    <section class="section first-section" style="background-image: url('hero_bg.jpg'); background-size: cover;">
        <div class="hero-wrap">
            <div class="logo">
                <img src="logo.png" alt="Vyadd" >

            </div>
            <h1 class="hero_title">
                Your <br>
                victorious <br>
                ads <br>
            </h1>
            <a href="" class="signup-link">Sign up</a>
        </div>
    </section>

    <section id="about" class="section second-section colored-bg">
        <div class="container">
            <div class="text-wrap">
                <div class="w50p left">
                    <p>We are here for you to make your advertising work. For several years we carefully studied the mobile advertising market, learned all its secrets, strengths and weaknesses. We always pay great attention to the subtleties and nuances, and due to this, we can direct the mobile advertising as quickly and efficiently as possible</p>
                </div>
                <div class="w50p right">
                    <p>We provide our customers with a high-quality service based on the latest technologies and in-depth data analytics. The customization and flexibility of our platform will allow you to discover the new highest standards for CPI / CPА / CPM advertising campaigns.</p>
                </div>
                <div class="w50p left">
                    <p>The team of our highly motivated specialists of the highest level is ready to immediately help you, answer all the questions that arise and help both advertisers and publishers achieve maximum results in a short time.</p>
                </div>
            </div>
        </div>
    </section>

    <section id="publishers" class="section third-section">
        <div class="container">
            <div class="text-wrap">
                <div class="w50p right" id="yellow-hover">
                    <p>The main goal of each publisher is to adjust the flow of their mobile traffic so that they bring the maximum profit. Do you still think that this is easier said than done? You tried a lot of options, but none of the platforms brought you the planned result? Do you think that other publishers are successful because of some magic or a secret recipe? We know this secret and can make it work for you.</p>
                </div>
                <div class="w50p left" id="green-hover">
                    <p>Working with us you choose only among the best offers. We will provide you with the most accurate and up-to-date tracking system in real time and API integration in a few easy steps. Our team of experts is always here to help.</p>
                </div>
                <div class="bg-text">
                    <span class="bg-text-first">For</span> <br>
                    <span class="bg-text-second">publishers</span>
                </div>
            </div>
        </div>
    </section>


    <section id="advertisers" class="section fourth-section colored-bg">
        <div class="container">
            <div class="text-wrap">
                <div class="w50p right">
                    <p>To deliver your Ads to every potential customer. To hook on the narrowest niches and the most hard-to-reach audiences. Advertisers face these challenges every day and often they determine the success of the campaign. You think that you have tried everything, but the result still does not please you? We are here to come to your aid.</p>
                </div>
                <div class="w50p left">
                    <p>The most accurate targeting. Fine-tuned Ad campaigns. The best anti-fraud technologies. Direct access to the most successful publishers and high-priority traffic. All these factors will work for you, bring you market leadership and impressively increase your ROI.</p>
                </div>
            </div>
        </div>
    </section>

    <section id="contact" class="section fifth-section">
        <div class="container">
            <div class="text-wrap">
                <div class="w50p right">
                    <p>Are you a publisher and want to turn your traffic into gold? Are you an advertiser and want your advertising campaign to clearly reach the customers? In both cases, you are in the right place.</p>
                </div>
                <div class="w50p left">
                    <p>Do not delay, start making profits today! Just fill out the form below and our team will contact you as soon as possible.</p>
                </div>
                <div class="w50p right">
                    <form action="" id="signup" class="message-form">
                        <input type="text" placeholder="Your name">
                        <input type="email" placeholder="Email">
                        <textarea name="" id="" cols="30" rows="10" placeholder="Message"></textarea>
                        <button>Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>
<div id="modal" class="wrap-modal hide">

    <div class="inner-modal">
        <a class="close-modal">x</a>
        <h3 class="modal-title">To get special document just enter your e-mail below</h3>
        <form action="" id="subscribeForm" route="{{ route('document') }}" method="POST">
            <input type="text" name="name" class="name-input" placeholder="Your Name">
            <input type="email" name="email" class="email-input" placeholder="Email">
            <button id="submit-btn" class="sumbit-btn">
                I want document!
            </button>
        </form>
    </div>
</div>
<script src="{{ asset('js/main.js') }}"></script>
</body>
</html>