@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                        <form action="" id="sendForm" route="{{ route('message') }}" method="POST">
                            <textarea class="form-control" name="message" id="message" cols="30" rows="10" placeholder="To tag name print {NAME}"></textarea>
                            <br>
                            <button id="submit" class="btn btn-primary">
                                I want document!
                            </button>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
