@if (strpos($message_text, '{NAME}'))
    @php($message_text = str_replace('{NAME}', $name, $message_text)) {{ $message_text }}
@else {{ $message_text }}
@endif

