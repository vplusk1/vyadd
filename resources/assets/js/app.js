
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


$('#submit').click(function(event) {
	event.preventDefault();
	var query = {
		message: $('textarea#message').val()
	};
	var route = $('form#sendForm').attr('route');
	var token = document.head.querySelector('meta[name="csrf-token"]').content;

	$.ajax({
		headers: {'X-CSRF-TOKEN': token },
		data: query,
		type: "POST",
		url: route,
		success: function (resp) {
			alert('Message has been sent!');
			$('textarea#message').val('');
		},
		error: function (err) {
			console.log(err);
		}
	});
});