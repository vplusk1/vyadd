'use strict';

require('./bootstrap');

import jquery from 'jquery';

window.jQuery = jquery;
window.$ = jquery;

let prev = document.referrer;

if (prev.indexOf('banner') > 0) {
    setTimeout(function() {
        $('.wrap-modal').removeClass('hide');
        $('body').addClass('show-modal');
    }, 5000);
}

$(document).on('click', '#hamburger, .bg-top-mobile-menu', function () {
	$('body').toggleClass('show-top-menu');
	$('#hamburger').toggleClass('is-active');
	$('#nav').toggleClass('is-active');
});


$('#modal .inner-modal .close-modal').click(function () {
	$('.wrap-modal').addClass('hide');
	$('body').removeClass('show-modal');
});

$(document).ready(function () {
    $(document).on("scroll", onScroll);

    $('a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");

        $('nav .nav-link a').each(function () {
            $(this).removeClass('active');
            $(this).removeClass('active-second');
        });

        $(this).addClass('active');

        if (($(this).attr("href") == "#advertisers") || ($(this).attr("href") == '#about')) $(this).addClass("active-second");


        var target = this.hash,
            menu = target;
        var $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top+2
        }, 500, 'swing', function () {
            $(document).on("scroll", onScroll);
        });
    });
});

function onScroll(event){
    var scrollPos = $(document).scrollTop();
    $('nav .nav-link').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            $('nav nav-link a').removeClass("active");
            currLink.addClass("active");
            if ((currLink.attr("href") == "#advertisers") || (currLink.attr("href") == '#about')) currLink.addClass("active-second");
        } else {
            currLink.removeClass("active");
        }
    });

}

// email form submit

$('#submit-btn').click(function(event) {
    event.preventDefault();
    var query = {
        email: $('form#subscribeForm .email-input').val(),
			  name: $('form#subscribeForm .name-input').val()
    };
    var route = $('form#subscribeForm').attr('route');
    var token = document.head.querySelector('meta[name="csrf-token"]').content;

    $.ajax({
        headers: {'X-CSRF-TOKEN': token },
        data: query,
        type: "POST",
        url: route,
        success: function (resp) {
            console.log(resp);
					  $('form#subscribeForm .email-input').hide();
					  $('form#subscribeForm .name-input').hide();
					  $('#submit-btn').hide();
					  $('.modal-title').text('Thanky you for downloading!');
        },
        error: function (err) {
            console.log(err);
        }
    });
});